console.log("hello");

// VARIABLE AND CREATE ELEMENT
const section = document.querySelector("#single-page");
const divJoke = document.createElement("div");
divJoke.className = "container";
const button = document.createElement("button");
button.setAttribute("id", "wantJoke");

button.innerText = "Clique pour une réponse";

section.appendChild(divJoke);
divJoke.appendChild(button);

// LISTENER

button.addEventListener("click", getJoke);

//FUNCTION
const joke = document.createElement("p");
const gif = document.createElement("img");

async function getJoke(e) {
  e.preventDefault();
  const request = await fetch("https://yesno.wtf/api?ref=publicapis.dev");
  const data = await request.json();

  joke.className = "joke-data";
  joke.innerHTML = data.answer;

  gif.src = data.image;

  divJoke.appendChild(joke);
  divJoke.appendChild(gif);
}

setTimeout(() => {
  joke.remove();
  gif.remove();
}, 10000);
