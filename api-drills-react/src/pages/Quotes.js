import React from "react";
import Header from "../components/Header";
import Nav from "../components/Nav";

const Quotes = () => {
  return (
    <div>
      <Header />
      <Nav />
    </div>
  );
};

export default Quotes;
