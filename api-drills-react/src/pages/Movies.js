import React from "react";
import Header from "../components/Header";
import Nav from "../components/Nav";

const Movies = () => {
  return (
    <div>
      <Header />
      <Nav />
    </div>
  );
};

export default Movies;
