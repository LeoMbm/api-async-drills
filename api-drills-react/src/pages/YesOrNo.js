import React from "react";
import Header from "../components/Header";
import Nav from "../components/Nav";

const YesOrNo = () => {
  return (
    <div>
      <Header />
      <Nav />
    </div>
  );
};

export default YesOrNo;
