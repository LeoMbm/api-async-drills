import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Movies from "./pages/Movies";
import Quotes from "./pages/Quotes";
import YesOrNo from "./pages/YesOrNo";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<YesOrNo />}></Route>
        <Route path="/quotes" element={<Quotes />}></Route>
        <Route path="/movies" element={<Movies />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
