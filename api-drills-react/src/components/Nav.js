import React from "react";
import { NavLink } from "react-router-dom";

const Nav = () => {
  return (
    <div className="nav">
      <ul>
        <NavLink to="/">
          <li>YesOrNo</li>
        </NavLink>
        <NavLink to="/quotes">
          <li>Quotes</li>
        </NavLink>
        <NavLink to="/movies">
          <li>Movies</li>
        </NavLink>
      </ul>
    </div>
  );
};

export default Nav;
